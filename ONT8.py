import yaml
from openpyxl import load_workbook
import os.path
from bs4 import BeautifulSoup
from mws import MWS
import mysql.connector
import re
import math
import time
from datetime import datetime, timedelta
import shutil

shipment_from = {
'Name': 'CDC Warehouse',
'AddressLine1': '4721 Edison Ave',
'City': 'Chino',
'StateOrProvinceCode': 'CA',
'PostalCode': '91710',
'CountryCode': 'US',
}
target_keys = ['SellerSKU', 'DashSKU', 'Quantity'] #exclude ASIN https://docs.developer.amazonservices.com/en_US/fba_guide/FBAGuide_CreateInShipPlan.html
shipment_types = ['LTL', 'SP']
target_FC = 'ONT8'
conf = yaml.load(open('conf/src.yml'))
DEBUG = conf['debug']
max_pallet_count = 24
sku_info = {}
if(DEBUG):
    request_ids = {}

def get_replenishment_report():
    #load source configuration
    conf = yaml.load(open('conf/src.yml'))
    source_filename = conf[target_FC]['source_filename']
    if(os.path.isfile(source_filename)):
        wb = load_workbook(filename=source_filename, read_only=True)
        ws = wb['Send These Items']
        items = []
        headers = [c.value for c in next(ws.rows)]
        for row in ws.iter_rows(min_row=2):
            item = {}
            for key in target_keys:
                position = headers.index(key)
                if(0 > position or not row[position].value):
                    break
                item.update({key: row[position].value})
            if(item):
                items.append(item)
    else:
        print('Error: ' + source_filename + ' doesn\'t exist.')
        exit()
    return items

def make_shipment_plan(items, start_time):
    non_target_FC_items = []
    target_FC_items = []
    if_FC_targeted = {}
    conf = yaml.load(open('conf/mws.yml'))
    shipment_plan_params_params={'ShipFromAddress.Name': shipment_from['Name'],
    'ShipFromAddress.AddressLine1': shipment_from['AddressLine1'],
    'ShipFromAddress.City': shipment_from['City'],
    'ShipFromAddress.StateOrProvinceCode': shipment_from['StateOrProvinceCode'],
    'ShipFromAddress.PostalCode': shipment_from['PostalCode'],
    'ShipFromAddress.CountryCode': shipment_from['CountryCode'],
    'LabelPrepPreference': '',
    'AreCasesRequired': 'true',
    }
    for i, item in enumerate(items, start=1):
        sku_info0 = get_sku_info(item.get('DashSKU'))
        quantity_in_case = int(sku_info0.get('quantity_in_case'))
        for key in target_keys:
            if('Quantity' == key):
                quantity_roundup = lambda x: (math.ceil(int(x) / quantity_in_case)) * quantity_in_case
                shipment_plan_params_params.update({'InboundShipmentPlanRequestItems.member.' + str(i) + '.' + key: quantity_roundup(item[key])})
            else:
                shipment_plan_params_params.update({'InboundShipmentPlanRequestItems.member.' + str(i) + '.' + key: item[key]})
            shipment_plan_params_params.update({'InboundShipmentPlanRequestItems.member.' + str(i) + '.QuantityInCase': quantity_in_case})
    if(DEBUG):
        print('79|shipment_plan_params_params')
        print(shipment_plan_params_params)
    shipment_plan_params = MWS(
    access_key=conf['mws']['access_key'],
    secret_key=conf['mws']['secret_key'],
    merchant_id=conf['mws']['merchant_id'],
    marketplace_id=conf['mws']['marketplace_id'],
    section='Fulfillment',
    operation='CreateInboundShipmentPlan',
    params=shipment_plan_params_params)
    shipment_plan_response = shipment_plan_params.call()
    shipment_plan_response_soup = BeautifulSoup(shipment_plan_response, 'xml')
    #always write plan to file; todo
    output_file = open(' '.join([str(tag.string).strip() for tag in shipment_plan_response_soup.findAll('ShipmentId')]) + ' plan.xml', 'w')
    output_file.write(shipment_plan_response_soup.prettify())
    output_file.close()
    #debug    
    if(DEBUG):
        request_ids.update({'CreateInboundShipmentPlan': shipment_plan_response_soup.RequestId.string})
        print('99|items', end='=')
        print(items)
        print('101|shipment_plan_response')
        print(shipment_plan_response)
    #make item list
    if(shipment_plan_response_soup.find('InboundShipmentPlans')):
        for member in shipment_plan_response_soup.find('InboundShipmentPlans').children:
            if('member' == member.name):
                if_FC_targeted.update({member.DestinationFulfillmentCenterId.string: True})
                if(DEBUG):
                    print('99|another FC targeted=', end='=')
                    print(member.DestinationFulfillmentCenterId.string, end=';')
                    print('101|if_FC_targeted', end='=')
                    print(if_FC_targeted)
                for item in member.Items.children:
                    if('member' == item.name):
                        item = {
                        'ASIN': item.FulfillmentNetworkSKU.string,
                        'SellerSKU': item.SellerSKU.string,
                        'Quantity': item.Quantity.string,
                        #'DestinationFulfillmentCenterId': member.DestinationFulfillmentCenterId.string,
                        #'ShipmentId': member.ShipmentId.string,
                        #'RequestId': shipment_plan_response_soup.RequestId.string,
                        #'QuantityInCase': 12, #todo: get this properly
                        }
                        if(target_FC != member.DestinationFulfillmentCenterId.string):
                            non_target_FC_items.append(item)
                        else:
                            target_FC_items.append(item)
    #todo: non_target_FC_items to file
    #todo: target_FC_items to db
    else:
        print('\033[0;31;48mInboundShipmentPlans Error: ', end='')
        print(shipment_plan_response_soup.find('Message').string)
        print('\033[0;0;0m') #todo: trim color on following line
        exit()
        return False
    if(DEBUG and (non_target_FC_items or target_FC_items)):
        print()
        print('128|Run start time', end='=')
        print(start_time)
        print('130|Planned not to ' + target_FC, end="=")
        print(non_target_FC_items)
        print('132|Planned to ' + target_FC, end="=")
        print(target_FC_items)
    #retry no more than 8x
    all_target_FC = True
    #FYI: if_FC_targeted = {'ONT8': True, 'RIC2': True, 'CLT2': True, 'SMF3': True}
    for FC in if_FC_targeted.keys():
        if(FC != target_FC):
            all_target_FC = False
    if(all_target_FC):
        output_file = open(' '.join([str(tag.string).strip() for tag in shipment_plan_response_soup.findAll('ShipmentId')]) + ' plan.xml', 'w')
        output_file.write(shipment_plan_response_soup.prettify())
        output_file.close()
        return shipment_plan_response_soup
    if(time.time() - start_time > 120):
        print('Time\'s up, can\'t get all to ' + target_FC + ', use the 1st shipping plan')
        shipment_plan_params_params={'ShipFromAddress.Name': shipment_from['Name'],
        'ShipFromAddress.AddressLine1': shipment_from['AddressLine1'],
        'ShipFromAddress.City': shipment_from['City'],
        'ShipFromAddress.StateOrProvinceCode': shipment_from['StateOrProvinceCode'],
        'ShipFromAddress.PostalCode': shipment_from['PostalCode'],
        'ShipFromAddress.CountryCode': shipment_from['CountryCode'],
        'LabelPrepPreference': '',
        'AreCasesRequired': 'true',
        }
        quantity_roundup = lambda x: (math.ceil(int(x) / quantity_in_case)) * quantity_in_case
        for i, item in enumerate(items, start=1):
            for key in target_keys:
                quantity_in_case = int(get_sku_info(item['DashSKU'])['quantity_in_case'])
                if('Quantity' == key):
                    if(int(item['Quantity']) != quantity_roundup(item[key]) ):
                        print(item['DashSKU'] + '\'s qty ' + str(item['Quantity']) + ' => ' + str(quantity_roundup(item[key])))
                    shipment_plan_params_params.update({'InboundShipmentPlanRequestItems.member.' + str(i) + '.' + key: quantity_roundup(item[key])})
                else:
                    shipment_plan_params_params.update({'InboundShipmentPlanRequestItems.member.' + str(i) + '.' + key: item[key]})
                shipment_plan_params_params.update({'InboundShipmentPlanRequestItems.member.' + str(i) + '.QuantityInCase': quantity_in_case})
        shipment_plan_params = MWS(
        access_key=conf['mws']['access_key'],
        secret_key=conf['mws']['secret_key'],
        merchant_id=conf['mws']['merchant_id'],
        marketplace_id=conf['mws']['marketplace_id'],
        section='Fulfillment',
        operation='CreateInboundShipmentPlan',
        params=shipment_plan_params_params)
        shipment_plan_response = shipment_plan_params.call()
        shipment_plan_response_soup = BeautifulSoup(shipment_plan_response, 'xml')
        output_file = open(' '.join([str(tag.string).strip() for tag in shipment_plan_response_soup.findAll('ShipmentId')]) + ' plan timeup.xml', 'w')
        output_file.write(shipment_plan_response_soup.prettify())
        output_file.close()
        return shipment_plan_response_soup
    else:
        print('177|try shipping plan again, time\'s left')
        time.sleep(1)
        return make_shipment_plan(target_FC_items, start_time)

"""
def check_shipment(shipment_plan):
    for member in shipment_plan.find('InboundShipmentPlans').children:
        if('member' == member.name):
            if(target_FC == member.find('DestinationFulfillmentCenterId').string):
                items = member.Items
                for i_item, item in enumerate(items.find_all('member')):
                    sku_info = get_sku_info(sku)
                    print('item: ' + str(i_item) + '/' + str(len(items)), end='; ')
                    print('item(l,w,h,v,wt): ' + sku_info, end='; ')
                    print('pallet(v,wt)', end='; ')
"""

def make_shipments(shipment_plan, items):
    shipment_ids = []
    shipment_target_keys = ['SellerSKU', 'Quantity']
    shipment_params_target_keys = ['SellerSKU', 'QuantityShipped']
    dashskus = get_dashskus(items)
    for member in shipment_plan.find('InboundShipmentPlans').children:
        if('member' == member.name):
            items = member.Items
            shipment_params_params={
            'InboundShipmentHeader.ShipmentName': 'rc ' + str(member.find('ShipmentId').string).strip(),
            'ShipmentId': str(member.find('ShipmentId').string).strip(),
            'InboundShipmentHeader.ShipFromAddress.Name': shipment_from['Name'],
            'InboundShipmentHeader.ShipFromAddress.AddressLine1': shipment_from['AddressLine1'],
            'InboundShipmentHeader.ShipFromAddress.City': shipment_from['City'],
            'InboundShipmentHeader.ShipFromAddress.StateOrProvinceCode': shipment_from['StateOrProvinceCode'],
            'InboundShipmentHeader.ShipFromAddress.PostalCode': shipment_from['PostalCode'],
            'InboundShipmentHeader.ShipFromAddress.CountryCode': shipment_from['CountryCode'],
            'InboundShipmentHeader.LabelPrepPreference': '',
            'InboundShipmentHeader.DestinationFulfillmentCenterId': str(member.find('DestinationFulfillmentCenterId').string).strip(),
            'InboundShipmentHeader.IntendedBoxContentsSource': 'NONE',
            'InboundShipmentHeader.AreCasesRequired': 'true',
            }
            for i_item, item in enumerate(items.find_all('member')):
                for i_shipment_target_key, shipment_target_key in enumerate(shipment_target_keys):
                    shipment_params_params.update({'InboundShipmentItems.member.' + str(i_item + 1) + '.' + shipment_params_target_keys[i_shipment_target_key]: str(item.find(shipment_target_key).string).strip()})
                dashsku = dashskus.get(item.SellerSKU)
                if(DEBUG):
                    print('236|for shipment ' + member.ShipmentId.string + ': sellersku', end='=')
                    print(item.SellerSKU, end='')
                    print('dashsku', end='=')
                    print(dashsku)
                sku_info = get_sku_info(dashsku)
                print(sku_info)
                quantity_in_case = sku_info.get('quantity_in_case', 0)
                shipment_params_params.update({'InboundShipmentPlanRequestItems.member.' + str(i_item + 1) + '.QuantityInCase': quantity_in_case})
            conf = yaml.load(open('conf/mws.yml'))
            if(DEBUG):
                print()
                print('196|CreateInboundShipment params:')
                print(shipment_params_params)
            shipment_params = MWS(
            access_key=conf['mws']['access_key'],
            secret_key=conf['mws']['secret_key'],
            merchant_id=conf['mws']['merchant_id'],
            marketplace_id=conf['mws']['marketplace_id'],
            section='Fulfillment',
            operation='CreateInboundShipment',
            params=shipment_params_params)
            shipment_response = shipment_params.call()
            shipment_response_soup = BeautifulSoup(shipment_response, 'xml')
            if(DEBUG):
                request_ids.update({'CreateInboundShipment': shipment_response_soup.RequestId.string})
            #output result
            if(shipment_response_soup and shipment_response_soup.CreateInboundShipmentResponse):
                print('212|shipment ' + shipment_response_soup.ShipmentId.string + ' made.')
                shipment_ids.append(True)
            else:
                print('215|error:', end='=')
                print(shipment_response_soup.prettify())
                shipment_ids.append(False)
    return shipment_ids

def make_pallets(shipment_plan, shipment_id, items):
    shipments_cartons = get_cartons(shipment_plan)
    for shipment in shipments_cartons:
        if(shipment.get('shipment_id') == shipment_id):
            boxes = shipment.get('cartons')
    shipments_pallets = []
    pallet_vol = 62 #ft3
    pallet_weight = 35 #lbs, of pallet (stringer, deck)
    dash_skus = get_dashskus(items)
    for shipment in shipments_cartons:
        if(str(shipment.get('shipment_id')).strip() == shipment_id):
            remainder = pallet_vol #empty ft3 left in current pallet, new pallet each new shipment
            shipment_pallet = {
            'box_count': len(boxes),
            'shipment_id': shipment_id,
            'pallets': [],
            'total_weight': 0,
            }
            pallet = [] #a pallet is a list of boxes
            total_weight = 0
            for i_box, box in enumerate(boxes, start=1): #maybe start at 0
                dash_sku = dash_skus.get(box.get('seller_sku'))
                sku_info = get_sku_info(dash_sku)
                if(sku_info):
                    cube = sku_info.get('cube')
                else:
                    print('WARNING: ' + seller_sku + ' has no cube')
                    print(sku_info)
                    return False
                total_weight += sku_info.get('wgt')
                if(cube <= remainder):
                    pallet.append(box)
                    remainder -= cube
                    if(DEBUG):
                        print('260|pallet_ct=' + str(len(shipment_pallet['pallets']) + 1), end=';')
                        print('cube=' + str(cube), end=';')
                        print('remainder=' + str(remainder))
                else:
                    shipment_pallet['pallets'].append(pallet)
                    pallet = []
                    pallet.append(box)
                    remainder = pallet_vol - cube
                    total_weight += pallet_weight
                    if(DEBUG):
                        print('270|pallet n=' + str(len (shipment_pallet['pallets']) + 1) + ' w_cumul=' + str(total_weight))
                if(len(boxes) == i_box): #last box was added (to pallet), stop here
                    shipment_pallet['pallets'].append(pallet)
                    if(DEBUG):
                        print('274|last box_ct=' + str(len(shipment_pallet['pallets'])))
                if(len(shipment_pallet['pallets']) == max_pallet_count): #last pallet was added (to pallets) for FTL, stop here
                    print('276|max pallets hit, FT full')
                    shipment_pallet.update({'total_weight': total_weight})
                    shipments_pallets.append(shipment_pallet)
                    return shipments_pallets
            shipment_pallet.update({'total_weight': total_weight})
            shipments_pallets.append(shipment_pallet)
    return shipments_pallets

#warning: code may be behind business' latest decision
#todo: handle the idea to ship all vs not
def error(error_data):
    #print('Shipment exceed 1 truckload\'s ' + str(max_pallet_count) + ' pallets. See error col in `' + target_FC + '.xlsx`.')
    item_quantities = {} #quantities of items shipped on made FTL
    if('pallets' in error_data): #todo: for boxes not pallets
        for pallet in error_data.get('pallets'): #add [max_pallet_count:] if over Truck 1
            for box in pallet:
                key = box.get('seller_sku')
                item_quantities.update({key: (item_quantities.get(key, 0) + box.get('quantity_in_case'))})
        """
        #this comment block is only to collect qty of items not in truck 1
        for pallet in error_data['pallets']
            for box in pallet:
                key = box['seller_sku']
                item_quantities.update({key: (item_quantities.get(key, 0) + box['quantity_in_case'])})
        """
    if('boxes' in error_data):
        for box in error.data('boxes'):
            key = box.get('seller_sku')
            item_quantities.update({key: (item_quantities.get(key, 0) + box.get('quantity_in_case'))})
    #for now, this is the only section that works per latest business idea; we get the whole shipping plan as BS4
    if(shipment_plan.CreateInboundShipmentPlanResponse):
        print('error() received a shipment plan')
    #write item_quantities into file
    conf = yaml.load(open('conf/src.yml'))
    source_filename = conf[target_FC]['source_filename']
    if(os.path.isfile(source_filename)):
        wb = load_workbook(filename=source_filename)
        ws = wb['Send These Items'] #todo: auto-src conf
        #headers = [c.value for c in next(ws.rows)]
        #start_col = len(headers)
        for seller_sku in item_quantities.keys():
            for row in ws.iter_rows():
                for cell in row: #todo: just col=4
                    #if(DEBUG):
                    #    print('257|' + str(cell.value) + '==' + seller_sku)
                    if(cell.value == seller_sku):
                        col_written = (4 + truck_ct - 1)
                        leftover = item_quantities.get(seller_sku, 0) - ws.cell(column = col_written, row = cell.row).value
                        ws.cell(column = col_written, row = cell.row, value = leftover)
                        if(DEBUG):
                            print('writing "' + str(leftover) + '" to cell(' + str(cell.row) + ',' + str(col_written), end=",")
                            print(str(cell.column) + ')')
                        #add header
                        ws.cell(column = col_written, row = 1, value='Qty not to ' + target_FC + ' in ' + error_data.get('shipment_id', '[no shipment id]'))
        wb.save(filename = source_filename)
    #todo: email warehouse at which carton to stop labelling, packing
    return item_quantities

def get_sku_info(dash_sku):
    if(dash_sku):
        #todo: read old xls format
        #todo: direct file access, copy, mount
        if(dash_sku in sku_info):
            return sku_info.get(dash_sku)
        else:
            wb = load_workbook('dash03.xlsx')
            ws = wb['DASH03']
            for row in ws.iter_rows(values_only=True):
                if(row[0] == dash_sku):
                    sku_info0 =  {
                    'cube': float(row[25]),
                    'quantity_in_case': int(row[5]),
                    'wgt': float(row[6]),
                    'landed_price': float(row[9]),
                    'len': float(row[22]),
                    'wid': float(row[23]),
                    'hgt': float(row[24]),
                    }
                    sku_info.update({dash_sku: sku_info0})
                    return sku_info0
    return {}

def write_db(cursor, connection, action, arg_data):
    if('add_to_truck' == action):
        query = ('INSERT INTO `trucks` (`truck_id`, `shipment_id`, `seller_sku`, `cube`) VALUES(%s, %s, %s, %s)')
        query_data = [arg_data.get('truck_id', 1),
        arg_data.get('shipment_id', ''),
        arg_data.get('seller_sku', ''),
        arg_data.get('cube', 0)]
        try:
            cursor.execute(query, query_data)
        except mysql.connector.Error as e:
            print('\033[0;31;48mmySQL Error: ', end='')
            print(e)
            print('\033[0;0;0m') #todo: trim color on following line
        connection.commit()

def get_cartons(shipment_plan):
    dashskus = get_dashskus(items)
    pallet_size = 62 #ft2 per pallet
    shipments_cartons = []
    for member in shipment_plan.find('InboundShipmentPlans').children:
        if('member' == member.name):
            shipment = {
            'shipment_id': str(member.find('ShipmentId').string).strip(),
            'cartons': []
            }
            total_weight = 0 #lbs
            items = member.Items
            for item in items.find_all('member'):
                seller_sku = str(item.find('SellerSKU').string).strip()
                quantity = int(item.find('Quantity').string)
                sku_info = get_sku_info(dashskus.get(seller_sku))
                print(sku_info)
                quantity_in_case = sku_info.get('quantity_in_case')
                if(1 == quantity_in_case and DEBUG):
                    print('WARNING: ' + seller_sku + ' is individually packed')
                #todo: check oversize https://sellercentral.amazon.com/forums/t/what-is-considered-an-oversize-unit/3460/4
                carton_count = divmod(quantity, quantity_in_case)
                #print(str(quantity) + ',' + str(quantity_in_case), end=',')
                #print(carton_count)
                for i in range(1, carton_count[0] + 1):
                    new_carton = {
                    'carton_id': seller_sku[2:] + 's' + str(i) + 'of' + str(carton_count[0]),
                    'seller_sku': seller_sku,
                    'quantity_shipped': quantity_in_case,
                    'quantity_in_case': quantity_in_case,
                    'len': sku_info.get('len'),
                    'wid': sku_info.get('wid'),
                    'hgt': sku_info.get('hgt'),
                    'wgt': sku_info.get('wgt'),
                    }
                    total_weight += sku_info.get('wgt')
                    #print(new_carton)
                    shipment['cartons'].append(new_carton)
                    shipment.update({'total_weight': total_weight})
            shipments_cartons.append(shipment)
    return shipments_cartons

def submit_carton(shipment_plan):
    feed_submission_ids = []
    shipments_cartons = get_cartons(shipment_plan)
    for carton_contents in shipments_cartons:
        #make feed file
        from xml.etree import ElementTree
        from xml.etree.ElementTree import Element
        from xml.etree.ElementTree import SubElement
        amazon_envelope = Element('AmazonEnvelope', {'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance', 'xsi:noNamespaceSchemaLocation': 'amzn-envelope.xsd'})
        header = SubElement(amazon_envelope, 'Header')
        document_version = SubElement(header, 'DocumentVersion')
        document_version.text = '1.00'
        merchant_identifier = SubElement(header, 'MerchantIdentifier')
        conf = yaml.load(open('conf/mws.yml'))
        merchant_identifier.text = conf['mws']['merchant_id']
        message_type = SubElement(amazon_envelope, 'MessageType')
        message_type.text = 'CartonContentsRequest'
        message = SubElement(amazon_envelope, 'Message')
        message_id = SubElement(message, 'MessageID')
        message_id.text = '1'
        carton_contents_request = SubElement(message, 'CartonContentsRequest')
        shipment_id = SubElement(carton_contents_request, 'ShipmentId')
        shipment_id.text = carton_contents['shipment_id']
        num_cartons = SubElement(carton_contents_request, 'NumCartons')
        num_cartons.text = str(len(carton_contents['cartons']))
        for carton in carton_contents['cartons']:
            carton_el = SubElement(carton_contents_request, 'Carton')
            carton_id = SubElement(carton_el, 'CartonId')
            carton_id.text = carton['carton_id']
            item = SubElement(carton_el, 'Item')
            seller_sku = SubElement(item, 'SKU')
            seller_sku.text = carton['seller_sku']
            quantity_shipped = SubElement(item, 'QuantityShipped')
            quantity_shipped.text = str(carton['quantity_in_case'])
            quantity_in_case = SubElement(item, 'QuantityInCase')
            quantity_in_case.text = str(carton['quantity_in_case'])
        str_amazon_envelope = '<?xml version="1.0" encoding="utf-8"?>' + ElementTree.tostring(amazon_envelope).decode("utf-8")
        if(DEBUG):
            #write to file
            output_file = open(carton_contents['shipment_id'] + ' carton.xml', 'w')
            output_file.write(str_amazon_envelope)
            output_file.close()
        #make md5 for signature
        from hashlib import md5
        m = md5()
        m.update(str_amazon_envelope.encode('utf-8'))
        import base64
        content_md5_value = base64.encodebytes(m.digest()).decode().strip('\n')
        submitfeed_params_params={
        'FeedType': '_POST_FBA_INBOUND_CARTON_CONTENTS_',
        'PurgeAndReplace': 'false',
        'ContentMD5Value': content_md5_value
        }
        conf = yaml.load(open('conf/mws.yml'))
        shipment_params = MWS(
        access_key = conf['mws']['access_key'],
        secret_key = conf['mws']['secret_key'],
        merchant_id = conf['mws']['merchant_id'],
        marketplace_id = conf['mws']['marketplace_id'],
        section = 'Feed',
        operation = 'SubmitFeed',
        params = submitfeed_params_params,
        body = str_amazon_envelope
        )
        submitfeed_response = shipment_params.call() #add FeedContent
        submitfeed_response_soup = BeautifulSoup(submitfeed_response, 'xml')
        if(DEBUG):
            request_ids.update({'SubmitFeed': submitfeed_response_soup.RequestId.string})
        if(submitfeed_response_soup.find('FeedProcessingStatus') and '_SUBMITTED_' == submitfeed_response_soup.find('FeedProcessingStatus').string):
            if(DEBUG):
                print('470|submitfeed_response_soup')
                print(submitfeed_response_soup)
            feed_submission_ids.append(str(submitfeed_response_soup.find('FeedSubmissionId').string))
        else:
            print(submitfeed_response_soup)
    return feed_submission_ids

def get_feed_submission_results(feed_submission_ids):
    feed_submission_results= []
    for feed_submission_id in feed_submission_ids:
        print('Requesting result of feed ' + str(feed_submission_id))
        feed_submission_params_params={
        'FeedSubmissionId': feed_submission_id,
        }
        conf = yaml.load(open('conf/mws.yml'))
        feed_submission_params = MWS(
        access_key = conf['mws']['access_key'],
        secret_key = conf['mws']['secret_key'],
        merchant_id = conf['mws']['merchant_id'],
        marketplace_id = conf['mws']['marketplace_id'],
        section = 'Feed',
        operation = 'GetFeedSubmissionResult',
        params = feed_submission_params_params,
        )
        feed_submission_response = feed_submission_params.call()
        feed_submission_response_soup = BeautifulSoup(feed_submission_response, 'xml')
        print(feed_submission_response_soup)
        if(DEBUG):
            if(feed_submission_response_soup.Code):
                if(feed_submission_response_soup.Code.string != 'FeedProcessingResultNotReady'):
                    request_ids.update({'GetFeedSubmissionResult': 'GetFeedSubmissionResult ' + feed_submission_response_soup.DocumentTransactionID.string + ' has no RequestId'})
            print()
            print('Feed Submission Result:')
            print(feed_submission_response_soup.find('Message').prettify())
        if(feed_submission_response_soup.find('MessagesWithError') and '0' == feed_submission_response_soup.find('MessagesWithError').string and 'Complete' == feed_submission_response_soup.find('StatusCode').string and int(feed_submission_response_soup.find('MessagesSuccessful').string) > 0):
            feed_submission_results.append(True)
        else:
            feed_submission_results.append(False)
    return feed_submission_results

def transporter(shipment_plan, items):
    shipment_ids = [str(tag.string).strip() for tag in shipment_plan.findAll('ShipmentId')]
    shipments_estimates = []
    #get estimates for all (2) shipment_types for each shipment_id
    for shipment_id in shipment_ids:
        #reset estimate related vars for (only) this shipment
        shipment_estimates = []
        cheapest_estimate = {}
        for shipment_type in shipment_types:
            transport_content = put_transport_content(shipment_plan, shipment_type, shipment_id, items)
            if(transport_content.get('status', False)):
                shipment_estimate = estimate_transport_request(shipment_id)
            else:
                shipment_estimate = {'detail': transport_content.get('detail', 'missing transport detail and/or status')}
                #print('no transport for id=' + shipment_plan.find('ShipmentId').string + '; type=' + shipment_type + '; detail=' + transport_content.get('detail'))
            shipment_estimates.append(shipment_estimate)
            shipments_estimates.append(shipment_estimate)
            #pick lower estimate's shipment_type, leave shipment set to that shipment_type
        cheapest_estimate = min( shipment_estimates, key=lambda estimate: estimate.get('currency_value', 0) )
        if(cheapest_estimate.get('shipment_type') != shipment_types[-1]):
            put_transport_content(shipment_plan, shipment_types[-1], shipment_id, items)
            estimate_transport_request(shipment_id) #makes the estimate visible in SC (web view)
    return shipments_estimates

def get_shipment_from_plan(shipment_plan, shipment_id):
    for member in shipment_plan.find('InboundShipmentPlans').children:
        if('member' == member.name and str(member.find('ShipmentId').string).strip() == shipment_id):
            return member
    return False

def put_transport_content(shipment_plan, shipment_type, shipment_id, items):
    partnered_str_type = {
    'SP': 'SmallParcel',
    'LTL': 'Ltl'
    }
    partnered_str_1_2 = 'TransportDetails.Partnered' + partnered_str_type[shipment_type] + 'Data.' #trailing period
    put_transport_content_params_params={
    'IsPartnered': 'true',
    'ShipmentType': shipment_type,
    partnered_str_1_2 + 'Contact.Name': 'Richard Chen',
    partnered_str_1_2 + 'Contact.Phone': '2126793022x209',
    partnered_str_1_2 + 'Contact.Email': 'richardc@jfranco.com',
    partnered_str_1_2 + 'Contact.Fax': '2126854864',
    partnered_str_1_2 + 'FreightReadyDate': (datetime.now() + timedelta(hours=168)).strftime('%Y-%m-%d'),
    partnered_str_1_2 + 'TotalWeight.Unit': 'pounds',
    }
    #get single shipment
    shipment = get_shipment_from_plan(shipment_plan, shipment_id)
    #make the call
    if(shipment and 'SP' == shipment_type):
        shipments_boxes = get_cartons(shipment_plan)
        for shipment in shipments_boxes:
            if(shipment.get('shipment_id') == shipment_id):
                if(shipment.get('total_weight') > 300 or len(shipment.get('cartons')) > 120):
                    return {
                    'status': False,
                    'detail': 'No ' + shipment_type + ' estimate as weight ' + str(shipment.get('total_weight')) + 'lbs and/or ' + str(len(shipment.get('cartons'))) + ' are too much.'
                    }
                put_transport_content_params_params.update({'ShipmentId': shipment.get('shipment_id')})
                put_transport_content_params_params.update({partnered_str_1_2 + 'BoxCount': len(shipment.get('cartons'))})
                put_transport_content_params_params.update({partnered_str_1_2 + 'TotalWeight.Value': shipment.get('total_weight')})
                for i_carton, carton in enumerate(shipment.get('cartons'), start = 1):
                    partnered_str = partnered_str_1_2 + 'PackageList.member.' + str(i_carton) + '.' #trailing period
                    put_transport_content_params_params.update({partnered_str + 'Weight.Value': str(carton.get('wgt'))})
                    put_transport_content_params_params.update({partnered_str + 'Weight.Unit': 'pounds'})
                    put_transport_content_params_params.update({partnered_str + 'Dimensions.Length': str(carton.get('len'))})
                    put_transport_content_params_params.update({partnered_str + 'Dimensions.Width': str(carton.get('wid'))})
                    put_transport_content_params_params.update({partnered_str + 'Dimensions.Height': str(carton.get('hgt'))})
                    put_transport_content_params_params.update({partnered_str + 'Dimensions.Unit': 'inches'})
                conf = yaml.load(open('conf/mws.yml'))
                put_transport_content_params = MWS(
                access_key = conf['mws']['access_key'],
                secret_key = conf['mws']['secret_key'],
                merchant_id = conf['mws']['merchant_id'],
                marketplace_id = conf['mws']['marketplace_id'],
                section = 'Fulfillment',
                operation = 'PutTransportContent',
                params = put_transport_content_params_params,
                )
                if(DEBUG):
                    print('560|put_transport_content_params_params')
                    print(put_transport_content_params_params)
                put_transport_content_response = put_transport_content_params.call()
                put_transport_content_response_soup = BeautifulSoup(put_transport_content_response, 'xml')
                if(DEBUG):
                    print('565|PutTransportContent Result:')
                    print(put_transport_content_response_soup)
                    #request_ids.update({'PutTransportContent': put_transport_content_response_soup.RequestId.string})
                return {
                'status': True,
                }
    if(shipment and 'LTL' == shipment_type):
        shipments_pallets = make_pallets(shipment_plan, shipment_id, items)
        print('653|shipments_pallets', end='=')
        print(shipments_pallets)
        total_weight = sum(shipment.get('total_weight') for shipment in shipments_pallets)
        if(total_weight < 150):
            return {
            'status': False,
            'detail': 'No ' + shipment_type + ' estimate as weight ' + str(total_weight) + ' doesn\'t meet 150lbs min.'
            }
        if(shipments_pallets):
            for shipment in shipments_pallets: #each shipment
                put_transport_content_params_params.update({'ShipmentId': shipment.get('shipment_id')})
                put_transport_content_params_params.update({partnered_str_1_2 + 'TotalWeight.Value': shipment.get('total_weight')})
                for i_pallet, pallet in enumerate(shipment['pallets'], start = 1):
                    partnered_str = partnered_str_1_2 + 'PalletList.member.' + str(i_pallet) + '.' #trailing period
                    put_transport_content_params_params.update({partnered_str + 'PalletNumber': i_pallet})
                    put_transport_content_params_params.update({partnered_str + 'Dimensions.Length': '40'})
                    put_transport_content_params_params.update({partnered_str + 'Dimensions.Width': '48'})
                    put_transport_content_params_params.update({partnered_str + 'Dimensions.Height': '60'})
                    put_transport_content_params_params.update({partnered_str + 'Dimensions.Unit': 'inches'})
                    put_transport_content_params_params.update({partnered_str + 'Weight.Value': ( int(shipment['total_weight']) / len(shipment['pallets']) ) }) #todo: calc in make_pallets
                    put_transport_content_params_params.update({partnered_str + 'Weight.Unit': 'pounds'})
                    put_transport_content_params_params.update({partnered_str + 'IsStacked': 'false'})
            conf = yaml.load(open('conf/mws.yml'))
            put_transport_content_params = MWS(
            access_key = conf['mws']['access_key'],
            secret_key = conf['mws']['secret_key'],
            merchant_id = conf['mws']['merchant_id'],
            marketplace_id = conf['mws']['marketplace_id'],
            section = 'Fulfillment',
            operation = 'PutTransportContent',
            params = put_transport_content_params_params,
            )
            if(DEBUG):
                print('693|put_transport_content_params_params')
                print(put_transport_content_params_params)
            put_transport_content_response = put_transport_content_params.call()
            put_transport_content_response_soup = BeautifulSoup(put_transport_content_response, 'xml')
            if(DEBUG):
                request_ids.update({'PutTransportContent': put_transport_content_response_soup.RequestId.string})
                print('699|PutTransportContent Result:')
                print(put_transport_content_response_soup)
            if(put_transport_content_response_soup.ErrorResponse):
                return {
                'status': False,
                'detail': put_transport_content_response_soup.Message.string,
                }
            else:
                return {
                'status': put_transport_content_response_soup.TransportStatus.string,
                }
    return {
    'status': False,
    'detail': 'Improper shipment_type of "' + shipment_type + '"',
    }

#only manually triggered
def confirm_transport(shipment_id):
    confirm_transport_params_params={
    'ShipmentId': shipment_id,
    }
    conf = yaml.load(open('conf/mws.yml'))
    confirm_transport_request_params = MWS(
    access_key = conf['mws']['access_key'],
    secret_key = conf['mws']['secret_key'],
    merchant_id = conf['mws']['merchant_id'],
    marketplace_id = conf['mws']['marketplace_id'],
    section = 'Fulfillment',
    operation = 'ConfirmTransportRequest',
    params = confirm_transport_params_params,
    )
    confirm_transport_response = confirm_transport_params.call()
    confirm_transport_response_soup = BeautifulSoup(confirm_transport_response, 'xml')
    if('ACCEPTED' == confirm_transport_response_soup.find('TransportStatus').string):
        print()
        print('\033[0;31;48mCONFIRM OR VOID/IGNORE TRANSPORT BEFORE ' + deadline + ' GMT') #todo: local time
        print('\033[0;0;0m')
        return True
    else:
        return False

def estimate_transport_request(shipment_id):
    estimate_transport_request_params_params={
    'ShipmentId': shipment_id,
    }
    conf = yaml.load(open('conf/mws.yml'))
    estimate_transport_request_params = MWS(
    access_key = conf['mws']['access_key'],
    secret_key = conf['mws']['secret_key'],
    merchant_id = conf['mws']['merchant_id'],
    marketplace_id = conf['mws']['marketplace_id'],
    section = 'Fulfillment',
    operation = 'EstimateTransportRequest',
    params = estimate_transport_request_params_params,
    )
    estimate_transport_request_response = estimate_transport_request_params.call()
    estimate_transport_request_response_soup = BeautifulSoup(estimate_transport_request_response, 'xml')
    if(DEBUG):
        print('753|estimate_transport_request_response_soup')
        print(estimate_transport_request_response_soup)
    if(estimate_transport_request_response_soup.find('TransportStatus') and 'ESTIMATING' == estimate_transport_request_response_soup.find('TransportStatus').string):
        print('Awaiting estimate for ' + shipment_id)
        time.sleep(10)
        get_transport_content_params = MWS(
        access_key = conf['mws']['access_key'],
        secret_key = conf['mws']['secret_key'],
        merchant_id = conf['mws']['merchant_id'],
        marketplace_id = conf['mws']['marketplace_id'],
        section = 'Fulfillment',
        operation = 'GetTransportContent',
        params = estimate_transport_request_params_params, #re-use aforementioned
        )
        get_transport_content_response = get_transport_content_params.call()
        get_transport_content_response_soup = BeautifulSoup(get_transport_content_response, 'xml')
        if('ESTIMATED' == get_transport_content_response_soup.find('TransportStatus').string and get_transport_content_response_soup.find('PartneredEstimate')):
            #print(shipment_id + '\'s ' + get_transport_content_response_soup.ShipmentType.string + ' estimate = ' + get_transport_content_response_soup.find('PartneredEstimate').find('Value').string + get_transport_content_response_soup.find('CurrencyCode').string)
            return {
            'shipment_id': shipment_id,
            'currency_value': float(get_transport_content_response_soup.find('PartneredEstimate').find('Value').string),
            'shipment_type': get_transport_content_response_soup.ShipmentType.string,
            }
    print(estimate_transport_request_response_soup)
    return {}

def get_labels(shipment_plan):
    shipments = get_cartons(shipment_plan)
    labels = []
    for shipment in shipments:
        cartons = shipment.get('cartons')
        shipment_id = shipment.get('shipment_id', '')
        if(DEBUG):
            print('GetUniquePackageLabels\' list of cartons in shipment :' + shipment_id)
            print(cartons)
        get_unique_package_labels_params_params={
        'ShipmentId': shipment_id,
        'PageType': 'PackageLabel_Letter_2',
        }
        get_unique_package_labels_params_body = get_unique_package_labels_params_params
        for i_carton, carton in enumerate(cartons, start = 1):
            get_unique_package_labels_params_body.update({'PackageLabelsToPrint.member.' + str(i_carton): carton['carton_id']})
            print(carton['carton_id'])
        conf = yaml.load(open('conf/mws.yml'))
        get_unique_package_labels_params = MWS(
        access_key = conf['mws']['access_key'],
        secret_key = conf['mws']['secret_key'],
        merchant_id = conf['mws']['merchant_id'],
        marketplace_id = conf['mws']['marketplace_id'],
        section = 'Fulfillment',
        operation = 'GetUniquePackageLabels',
        params = get_unique_package_labels_params_body,
        )
        get_unique_package_labels_response = get_unique_package_labels_params.call()
        get_unique_package_labels_response_soup = BeautifulSoup(get_unique_package_labels_response, 'xml')
        if(DEBUG):
            print('GetUniquePackageLabels Response:')
            print(get_unique_package_labels_response)
        #todo: check checksum
        if(get_unique_package_labels_response_soup.PdfDocument):
            coded_file = get_unique_package_labels_response_soup.PdfDocument.string
            import base64
            decoded_file = base64.b64decode(coded_file)
            src_filename = shipment_id + '.zip'
            new_file = open(src_filename, 'wb')
            new_file.write(decoded_file)
            new_file.close()
            from zipfile import ZipFile
            with ZipFile(src_filename, 'r') as zip_ref:
                zip_ref.extractall('')
            shutil.move('PackageLabels.pdf', shipment_id + ' label.pdf') #hardcoded filename since MWS uses same
            os.remove(src_filename)
            labels.append(shipment_id + ' label is ok')
        else:
            labels.append(get_unique_package_labels_response_soup.Message.string)
    return labels

def get_dashskus(items):
    dashskus = {}
    for item in items:
        dashskus.update({item.get('SellerSKU'): item.get('DashSKU')})
    return dashskus

def make_po(shipment_plan, items):
    #get dict of dashskus to sellerskus to save time
    dashskus = get_dashskus(items)
    #copy PO source to new destination
    dest_filename = ' '.join([str(tag.string).strip() for tag in shipment_plan.findAll('ShipmentId')]) + ' PO.xlsx'
    shutil.copy('PO.xlsx', dest_filename)
    wb = load_workbook(dest_filename)
    ws = wb['Sheet1']
    #data for xlsx file
    rows = []
    row_start = 2
    col_start = 3
    for member in shipment_plan.find('InboundShipmentPlans').children:
        if('member' == member.name):
            items = member.Items
            shipment_id = str(member.find('ShipmentId').string).strip()
            for item in items.find_all('member'):
                sellersku = str(item.find('SellerSKU').string).strip()
                row = []
                row.append('ONT8') #stor
                row.append(shipment_id) #CustPO
                row.append('') #dept
                row.append((datetime.now()).strftime('%m/%d/%y')) #start date
                row.append((datetime.now() + timedelta(hours=168)).strftime('%m/%d/%Y')) #cancel date
                row.append(dashskus.get( sellersku )) #style, our internal DashSKU
                row.append(str(item.find('FulfillmentNetworkSKU').string).strip()) #SKU, but it's actually ASIN
                row.append(int(str(item.find('Quantity').string).strip())) #QTY
                row.append('$' + str(get_sku_info(sellersku).get('landed_price'))) #Unit price
                row.append('LTL')
                rows.append(row)
            if(DEBUG):
                print('PO data:')
                print(rows)
            #write to file
            for i_row, row in enumerate(rows, start = row_start):
                for i_col in range(col_start, 13):
                    if(DEBUG):
                        print('(' + str(i_row - row_start), end=',')
                        print(str(i_col - col_start), end=');')
                    ws.cell(column = i_col, row = i_row, value=rows[i_row - row_start][i_col - col_start])
            wb.save(filename = dest_filename)
            return True
    return False

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--truck', nargs='?', const=1, type=int, default=1)
args = parser.parse_args()
truck_ct = int(args.truck) - 1
if(0 >= truck_ct or truck_ct > 4):
    print('Command argument "truck" out of range, setting to default (1)')
    truck_ct = 1

print('Making FT #' + str(truck_ct) + ' to ' + target_FC + '.')

items = get_replenishment_report()
start_time = time.time()
shipment_plan = make_shipment_plan(items, start_time)
if(shipment_plan):
    shipment_ids = make_shipments(shipment_plan, items)
    if(all(shipment_id for shipment_id in shipment_ids)):
        feed_submission_ids = submit_carton(shipment_plan)
        time.sleep(60)
        feed_results = get_feed_submission_results(feed_submission_ids)
        if(all(feed_result == True for feed_result in feed_results)):
            transport = transporter(shipment_plan, items)
            po = make_po(shipment_plan, items)
            labels = get_labels(shipment_plan)
        else:
            print('Solve feed error and re-run', end=':')
            print(feed_results)