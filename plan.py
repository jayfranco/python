from bs4 import BeautifulSoup
from mws import MWS
import yaml
import time
import sys

DEBUG = False

shipmentFrom = {
'Name': 'CDC Warehouse',
'AddressLine1': '4721 Edison Ave',
'City': 'Chino',
'StateOrProvinceCode': 'CA',
'PostalCode': '91710',
'CountryCode': 'US',
}
quantities = []

#write ShipmentPlan to db
def write_db(soup):
    cols_address = ['Name',
    'AddressLine1',
    'City',
    'StateOrProvinceCode',
    'PostalCode',
    'CountryCode']
    import mysql.connector
    conf = yaml.load(open('conf/aws-rds-mysql.yml'))
    connection = mysql.connector.connect(
        host=conf['plans-db']['host'],
        user=conf['plans-db']['user'],
        password=conf['plans-db']['passwd'],
        database=conf['plans-db']['db']
        )
    cursor = connection.cursor()
    query_plan = ('INSERT INTO `plans` (`RequestId`, `DestinationFulfillmentCenterId`, `SellerSKU`, `Quantity`, `ShipmentId`) VALUES(%s, %s, %s, %s, %s)')
    query_address = ('INSERT INTO `address` (`DestinationFulfillmentCenterId`, `Name`, `AddressLine1`, `City`, `StateOrProvinceCode`, `PostalCode`, `CountryCode`) VALUES(%s, %s, %s, %s, %s, %s, %s)')
    #read csv
    if('CreateInboundShipmentPlanResponse' == soup.name): #todo: if no data, no writing
        print('* writing InboundShipmentPlan to db ' + conf['plans-db']['host'])
        for member in soup.find('InboundShipmentPlans').children:
            if('member' == member.name):
                cols = ['DestinationFulfillmentCenterId',
                    'SellerSKU',
                    'Quantity',
                    'ShipmentId']
                data = [ soup.find('RequestId').string ]
                for col in cols:
                    data.append(member.find(col).string)
                cursor.execute(query_plan, data)

                #attempt writing address
                data = [ member.find('DestinationFulfillmentCenterId').string ]

                for col in cols_address:
                    data.append(member.find('ShipToAddress').find(col).string)
                try:
                    cursor.execute(query_address, data)
                    print('  * added FulfillmentCenter ' + member.find('ShipToAddress').find('DestinationFulfillmentCenterId').string)
                except:
                    print('  * FulfillmentCenter (' + member.find('ShipToAddress').find('DestinationFulfillmentCenterId').string + ') already saved')
                    print(sys.exc_info())
                connection.commit()
        print('* wrote InboundShipmentPlan to db')
    elif('UpdateInboundShipmentResponse' == soup.name):
        print('* UpdateInboundShipmentResponse read') #todo

#create shipment plan
if(not DEBUG):
    conf = yaml.load(open('conf/mws.yml'))
    if (len(sys.argv) > 1):
        quantities.append(int(sys.argv[1]))
    else:
        print('* using defaults: quantity(1)=88, ASIN=B07K4WPF4V, SKU=JF29682')
        quantities.append(88)
    shipmentPlanParams = MWS(
        access_key=conf['mws']['access_key'],
        secret_key=conf['mws']['secret_key'],
        merchant_id=conf['mws']['merchant_id'],
        marketplace_id=conf['mws']['marketplace_id'],
        section='Fulfillment',
        operation='CreateInboundShipmentPlan',
        params={'ShipFromAddress.Name': shipmentFrom['Name'],
        'ShipFromAddress.AddressLine1': shipmentFrom['AddressLine1'],
        'ShipFromAddress.City': shipmentFrom['City'],
        'ShipFromAddress.StateOrProvinceCode': shipmentFrom['StateOrProvinceCode'],
        'ShipFromAddress.PostalCode': shipmentFrom['PostalCode'],
        'ShipFromAddress.CountryCode': shipmentFrom['CountryCode'],
        'InboundShipmentPlanRequestItems.member.1.SellerSKU': 'JF29682',
        'InboundShipmentPlanRequestItems.member.1.ASIN': 'B07K4WPF4V',
        'InboundShipmentPlanRequestItems.member.1.Quantity': quantities[0],
        'LabelPrepPreference': '',
        })
    shipmentPlanResponse = shipmentPlanParams.call()
    shipmentPlanResponseSoup = BeautifulSoup(shipmentPlanResponse, 'xml')
else:
    with open('testing-plan.xml') as fp:
        shipmentPlanResponseSoup = BeautifulSoup(fp, 'xml')

#create shipment
if(len(shipmentPlanResponseSoup.findAll('ErrorResponse')) == 0):
    write_db(shipmentPlanResponseSoup)
    for member in shipmentPlanResponseSoup.find('InboundShipmentPlans').children:
        if('member' == member.name):
            #print('* Retrieved shipmentId=' + item.find('ShipmentId').string + '.')
            shipmentParams = MWS(
                access_key=conf['mws']['access_key'],
                secret_key=conf['mws']['secret_key'],
                merchant_id=conf['mws']['merchant_id'],
                marketplace_id=conf['mws']['marketplace_id'],
                section='Fulfillment',
                operation='CreateInboundShipment',
                params={'ShipmentId': member.find('ShipmentId').string,
                'InboundShipmentHeader.ShipmentName': 'rc ' + member.find('ShipmentId').string,
                'InboundShipmentHeader.ShipFromAddress.Name': shipmentFrom['Name'],
                'InboundShipmentHeader.ShipFromAddress.AddressLine1': shipmentFrom['AddressLine1'],
                'InboundShipmentHeader.ShipFromAddress.City': shipmentFrom['City'],
                'InboundShipmentHeader.ShipFromAddress.StateOrProvinceCode': shipmentFrom['StateOrProvinceCode'],
                'InboundShipmentHeader.ShipFromAddress.PostalCode': shipmentFrom['PostalCode'],
                'InboundShipmentHeader.ShipFromAddress.CountryCode': shipmentFrom['CountryCode'],
                'InboundShipmentHeader.DestinationFulfillmentCenterId': member.find('DestinationFulfillmentCenterId').string,
                'InboundShipmentItems.member.1.QuantityShipped': member.find('Quantity').string,
                'InboundShipmentItems.member.1.SellerSKU': member.find('SellerSKU').string,
                'LabelPrepPreference': member.find('LabelPrepType').string,
                })
            print('* making shipment ' + str(member.find('ShipmentId').string) + ' with params:')
            #write_db(shipmentResponse)
            shipmentResponse = shipmentParams.call()

            #if 2nd arg, make 2nd plan, check combinable, combine
            if (len(sys.argv) > 2):
                quantity = int(sys.argv[2])
                if(quantity != 0):
                    shipmentPlanParams = MWS(
                        access_key=conf['mws']['access_key'],
                        secret_key=conf['mws']['secret_key'],
                        merchant_id=conf['mws']['merchant_id'],
                        marketplace_id=conf['mws']['marketplace_id'],
                        section='Fulfillment',
                        operation='CreateInboundShipmentPlan',
                        params={'ShipFromAddress.Name': shipmentFrom['Name'],
                        'ShipFromAddress.AddressLine1': shipmentFrom['AddressLine1'],
                        'ShipFromAddress.City': shipmentFrom['City'],
                        'ShipFromAddress.StateOrProvinceCode': shipmentFrom['StateOrProvinceCode'],
                        'ShipFromAddress.PostalCode': shipmentFrom['PostalCode'],
                        'ShipFromAddress.CountryCode': shipmentFrom['CountryCode'],
                        'InboundShipmentPlanRequestItems.member.1.SellerSKU': member.find('SellerSKU').string,
                        'InboundShipmentPlanRequestItems.member.1.ASIN': 'B07K4WPF4V',
                        'InboundShipmentPlanRequestItems.member.1.Quantity': quantity,
                        'LabelPrepPreference': '',
                        })
                    shipmentPlanResponse = shipmentPlanParams.call()
                    shipmentPlanResponseSoup = BeautifulSoup(shipmentPlanResponse, 'xml')
                    #check combinable
                    if(member.find('DestinationFulfillmentCenterId').string == shipmentPlanResponseSoup.find('DestinationFulfillmentCenterId').string and member.find('LabelPrepType').string == shipmentPlanResponseSoup.find('LabelPrepType').string):
                        print('* combinable ' + member.find('ShipmentId').string + ' into ' + shipmentPlanResponseSoup.find('ShipmentId').string)
                        print('  * ' + member.find('DestinationFulfillmentCenterId').string + ' vs ' + shipmentPlanResponseSoup.find('DestinationFulfillmentCenterId').string)
                        print('  * ' + member.find('LabelPrepType').string + ' vs ' + shipmentPlanResponseSoup.find('LabelPrepType').string)
                        #combine
                        if(len(shipmentPlanResponseSoup.findAll('ErrorResponse')) == 0):
                            time.sleep(4)
                            shipmentParams = MWS(
                                access_key=conf['mws']['access_key'],
                                secret_key=conf['mws']['secret_key'],
                                merchant_id=conf['mws']['merchant_id'],
                                marketplace_id=conf['mws']['marketplace_id'],
                                section='Fulfillment',
                                operation='UpdateInboundShipment',
                                params={'ShipmentId': member.find('ShipmentId').string, #1st not 2nd ShipmentId
                                'InboundShipmentHeader.ShipmentName': 'rc2 ' + member.find('ShipmentId').string,
                                'InboundShipmentHeader.ShipFromAddress.Name': shipmentFrom['Name'],
                                'InboundShipmentHeader.ShipFromAddress.AddressLine1': shipmentFrom['AddressLine1'],
                                'InboundShipmentHeader.ShipFromAddress.City': shipmentFrom['City'],
                                'InboundShipmentHeader.ShipFromAddress.StateOrProvinceCode': shipmentFrom['StateOrProvinceCode'],
                                'InboundShipmentHeader.ShipFromAddress.PostalCode': shipmentFrom['PostalCode'],
                                'InboundShipmentHeader.ShipFromAddress.CountryCode': shipmentFrom['CountryCode'],
                                'InboundShipmentHeader.DestinationFulfillmentCenterId': member.find('DestinationFulfillmentCenterId').string,
                                'InboundShipmentItems.member.1.QuantityShipped': int(member.find('Quantity').string) + int(quantity),
                                'InboundShipmentItems.member.1.SellerSKU': member.find('SellerSKU').string,
                                })
                            shipmentResponse = shipmentParams.call()
                            if(len(BeautifulSoup(shipmentResponse, 'xml').findAll('ErrorResponse')) > 0):
                                print('* erroroneous UpdateInboundShipment to combine')
                                print(shipmentResponse)
                            else:
                                print('* successful UpdateInboundShipment to combine')
                    else:
                        print('* !combinable')
                        print('  * ' + member.find('DestinationFulfillmentCenterId').string + ' vs ' + shipmentPlanResponseSoup.find('DestinationFulfillmentCenterId').string)
                        print('  * ' + member.find('LabelPrepType').string + ' vs ' + shipmentPlanResponseSoup.find('LabelPrepType').string)
                else:
                    print('* quantity_2 is 0, skip 2nd call')
            else:
                print('* no 2nd arg thus no combining')
            
            if(len(BeautifulSoup(shipmentResponse, 'xml').findAll('ErrorResponse')) == 0):
                #cancel shipment
                quantity = 0
                time.sleep(4)
                shipmentParams = MWS(
                    access_key=conf['mws']['access_key'],
                    secret_key=conf['mws']['secret_key'],
                    merchant_id=conf['mws']['merchant_id'],
                    marketplace_id=conf['mws']['marketplace_id'],
                    section='Fulfillment',
                    operation='UpdateInboundShipment',
                    params={'ShipmentId': member.find('ShipmentId').string,
                    'InboundShipmentHeader.ShipmentName': 'rcx ' + member.find('ShipmentId').string,
                    'InboundShipmentHeader.ShipFromAddress.Name': shipmentFrom['Name'],
                    'InboundShipmentHeader.ShipFromAddress.AddressLine1': shipmentFrom['AddressLine1'],
                    'InboundShipmentHeader.ShipFromAddress.City': shipmentFrom['City'],
                    'InboundShipmentHeader.ShipFromAddress.StateOrProvinceCode': shipmentFrom['StateOrProvinceCode'],
                    'InboundShipmentHeader.ShipFromAddress.PostalCode': shipmentFrom['PostalCode'],
                    'InboundShipmentHeader.ShipFromAddress.CountryCode': shipmentFrom['CountryCode'],
                    'InboundShipmentHeader.DestinationFulfillmentCenterId': member.find('DestinationFulfillmentCenterId').string,
                    'InboundShipmentHeader.ShipmentStatus': 'CANCELLED',
                    })
                shipmentResponse = shipmentParams.call()
                if(len(BeautifulSoup(shipmentResponse, 'xml').findAll('ErrorResponse')) == 0):
                    print('* cancelled shipment ' + member.find('ShipmentId').string)
                else:
                    print('* error during CreateInboundShipment(Cancel)')
            else:
                print('* error during CreateInboundShipment:')
                print(shipmentResponse)
else:
    print('* error during CreateShipmentPlan')