FROM python:3
WORKDIR /home
RUN pip install --no-cache-dir openpyxl pyyaml requests mysql-connector-python
RUN pip install --no-cache-dir lxml beautifulsoup4
RUN apt-get update \
    && apt-get install -y \
        vim
