# Is an ASIN in Catalog
Given list of ASINs, return which aren't in the catalog.

## Run containers individually
When run solo, it's mostly a python dev container.

Build
```
cd /that/dockerfile/folder
docker build -t [IMAGE NAME, usually prefix 'jf'] .
```
Run with shared folders
```
docker run -d -it -v ~/Documents/asin-in-catalog:/home [IMAGE from `docker images`]
```
Shell into docker instance (or replace `bash` with your command)
```
docker exec -it [CONTAINER from `docker ps`] bash
```
Copy needed file in
```
docker cp /Volumes/dash/dash07JD.xlsx f3517d391c0a:/home 
```
Do your usual work
```
vi asin-in-catalog.py
python asin-in-catalog.py
```

## Start Volume for Persistent DB
```
docker volume create mysql-data
docker volume ls
docker volume inspect mysql-data
cd db
docker build -t jfdb-image .
docker run --name jfdb-container -v mysql-data:/var/lib/mysql -d jfdb-image
#sudo -u install ls /var/lib/docker/volumes/mysql-data/_data #fails without ICCS giving admin

#test volume persists
docker exec -it [CONTAINER from `docker ps`] bash
/usr/bin/mysql -p -A
#write test data
INSERT INTO `catalog_1564002580` (`item-name`,`asin1`) VALUES ('fake item','B97K4WPF4Z');
INSERT INTO `shipments` (`shipment_id`,
        `request_id`,
        `ASIN`,
        `SKU`,
        `quantity`,
        `destination_fulfillment_center_id`)
        VALUES (`shipment_id`,
        `requestey`,
        `B97K4WPF4Z`,
        `JF47623FF`,
        6786,
        `SAV7`)

#remove container
docker stop [CONTAINER]
docker exec -it [CONTAINER] bash #should fail

#new container
#docker run above
docker start [CONTAINER from `docker ps`]

#check test data still there, should be
docker exec -it [CONTAINER from prior]
/usr/bin/mysql -p -A
SELECT * FROM `catalog_1564002580`;
SELECT * FROM `shipments`;
```
* [source](http://www.ethernetresearch.com/docker/docker-tutorial-persistent-storage-volumes-and-stateful-containers/)


## Run as Docker Compose
Needed if you want the database, too, yet not to run that container manually.
```
docker-compose up --force-recreate
docker run [IMAGE-ID-HERE from `docker images`]
docker-compose run CONTAINER-NAME-HERE bash
```

## Todo
1. add [amazon-msw-python](https://bitbucket.org/richardpenman/amazon-mws-python/src/default/) as [subrepo](https://www.mercurial-scm.org/wiki/Subrepository)
1. add `hg`
1. add `pep8`
1. add deployment/CI
* cache
* no write if exist
* `try/catch` errors
* timestamped table names?
* input row[0] not always there?
* always active sheet? doc included this note

# Truckload Builder
Added this functionality here and not as a sub-repo since the containers are the same.

## Data(base)
```
cd ~/Documents/asin-in-catalog
docker-compose run catalog-db bash
/usr/bin/mysql -h HOSTNAME -u USERNAME -p jf #vars from conf/aws-rds-mysql.yml

#retrieve everything
SELECT * FROM `plans`
  ORDER BY `RequestId`,
  `created`,
  `DestinationFulfillmentCenterId`;

#formatted
SELECT `SellerSKU` `What`,
  LEFT(`RequestId`, 4) `Who`,
  `ShipmentId` `Who`,
  GROUP_CONCAT(`DestinationFulfillmentCenterId` ORDER BY `DestinationFulfillmentCenterId` ASC SEPARATOR ', ') `Where`,
  GROUP_CONCAT(`Quantity` SEPARATOR ', ') `How`,
  date_format(`created`, '%c/%d %H:%i') `When`
  # `ShipmentID` `What(Shipment)`,
  FROM `plans`
  GROUP BY `RequestId`
  ORDER BY created DESC;

```

## Two Workflows
1. create plan, create shipment, cancel it
1. create plan, create shipment, create plan, check combinable, combine, cancel all

# ONT8
Make a truckload for ONT8 only. Manually handle all other FCs.

# For Developers
* helpful reading for work on these projects
  * [openpyxl](https://openpyxl.readthedocs.io/en/stable/tutorial.html#values-only)
  * [BeautifulSoup4](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)
  * anything Docker
  * [requests](https://2.python-requests.org/en/master/)