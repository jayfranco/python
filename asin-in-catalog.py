import yaml
from mws import MWS
from openpyxl import load_workbook
import os.path
from xml.etree import ElementTree as ET
from bs4 import BeautifulSoup

#load MWS configuration
conf = yaml.load(open('conf/mws.yml'))
merchant_id = conf['mws']['merchant_id']
access_key = conf['mws']['access_key']
secret_key = conf['mws']['secret_key']
marketplace_id = conf['mws']['marketplace_id']

#load source configuration
conf = yaml.load(open('conf/src.yml'))
source_filename = conf['src']['source_filename']
if os.path.isfile(source_filename):
    fileOpen = open(source_filename)
else:
    print('Error: ' + source_filename + ' doesn\'t exist.')
    exit()

ASINs = []
DEBUG = conf['debug']

#read, store ASINs from xls
wb = load_workbook(source_filename)
ws = wb.active
for row in ws.iter_rows(values_only=True):
    ASINs.append(row[0])

#prepare MWS call
def call_mws(access_key, secret_key, merchant_id, marketplace_id, section, operation, params=dict()):
    mws = MWS(access_key=access_key, secret_key=secret_key, merchant_id=merchant_id, marketplace_id=marketplace_id, section=section, operation=operation, params=params)
    response = mws.call()
    return response

#get field (needle) from XML (haystack)
def get_field(haystack, needle):
    if(DEBUG):
        if('ReportRequestId' == needle):
            return '117378018101'
        if('GeneratedReportId' == needle):
            return '15860639976018101'
    parsed = BeautifulSoup(haystack, 'xml')
    needle_find = parsed.find(needle)
    if(DEBUG):
        print('haystack')
        print(haystack)
    if(needle_find.string):
        return needle_find.string
    else:
        return 'error'

#query db against said ASINs
def query_db(ASINs):
    print('ASINs not in catalog')
    print('--------------------')
    for asin in ASINs:
        query = ("SELECT `asin1`, COUNT(`asin1`) AS `count_asin` FROM `catalog_1564002580` WHERE `asin1` = %s")
        cursor.execute(query, (asin,))
        for (asin1, count_asin) in cursor:
            if count_asin == 0:
                print(asin)
    cursor.close()
    connection.close()
    return 1

#get all ASINs
report_request_response = call_mws(access_key, secret_key, merchant_id, marketplace_id, 'Reports', 'RequestReport', {'ReportType': '_GET_MERCHANT_LISTINGS_ALL_DATA_'})
report_request_id_1 = get_field(report_request_response, 'ReportRequestId')
if(DEBUG):
    print('report_request_id_1')
    print(report_request_id_1)
report_id_response = call_mws(access_key, secret_key, merchant_id, marketplace_id, 'Reports', 'GetReportRequestList', {'ReportRequestIdList.Id.1': report_request_id_1})
report_id = get_field(report_id_response, 'GeneratedReportId')
if(DEBUG):
    print('report_id')
    print(report_id)
report_response = call_mws(access_key, secret_key, merchant_id, marketplace_id, 'Reports', 'GetReport', {'ReportId': report_id})
if(DEBUG):
    print('report_response')
    print(report_response)

#write csv report to db
def write_db:
    import mysql.connector

    conf = yaml.load(open('conf/catalog-db.yml'))

    connection = mysql.connector.connect(
        host=conf['catalog-db']['host'],
        user=conf['catalog-db']['user'],
        password=conf['catalog-db']['passwd'],
        database=conf['catalog-db']['db']
        )
    cursor = connection.cursor()
    timestamp = '1564002580'
    query = ('INSERT INTO `catalog_`' + timestamp + ' (`item-name`, `asin1`) VALUES(%s, %s)')

    #read csv
    import csv

    report_file_name = 'report.txt'
    with open(report_file_name) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter='\t')
        line_count = 0
        for row in csv_reader:
            data = (row[0], row[16])
            cursor.execute(query, data)
    connection.commit()

catalog = write_db()

#query all ASIN's for if in catalog
query_db(ASINs)
