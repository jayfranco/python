import yaml

stream = open('conf/notify.yml', 'r')
notify_config_yml = yaml.load(stream)

clientId = notify_config_yml['email']['clientId']
secretId = notify_config_yml['email']['secretId']
toEmail = notify_config_yml['email']['toEmail']
scriptName = 'ASIN in Catalog'
statusShort = 'OK'
statusLong = 'Completed successfully'

#send
from O365 import Account

credentials = (clientId, secretId)
account = Account(credentials)
if not account.is_authenticated:
    account.authenticate(scopes=['message_send'])
m = account.new_message()
m.to.add(toEmail)
m.subject = scriptName + ': ' + statusShort
m.body = statusLong
m.send()
