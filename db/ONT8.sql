USE `catalog`;

CREATE TABLE `shipments` (
	`shipment_id` TEXT,
	`request_id` TEXT,
	`ASIN` TEXT,
	`SKU` TEXT,
	`quantity` MEDIUMINT,
    `destination_fulfillment_center_id` CHAR(4)
);

CREATE TABLE `plans` (
  `RequestId` TEXT,
  `DestinationFulfillmentCenterId` TEXT,
  `SellerSKU` TEXT,
  `Quantity` INT,
  `ShipmentId` TEXT,
  `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `address` (
  `Name` TEXT,
  `DestinationFulfillmentCenterId` VARCHAR(4),
  `AddressLine1` VARCHAR(255),
  `City` VARCHAR(255),
  `StateOrProvinceCode` VARCHAR(2),
  `PostalCode` VARCHAR(10),
  `CountryCode` VARCHAR(2) DEFAULT 'US',
  `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT FulfullmentCenters UNIQUE(`DestinationFulfillmentCenterId`, `AddressLine1`, `City`, `StateOrProvinceCode`, `PostalCode`, `CountryCode`)
);

CREATE TABLE `trucks` (
  `truck_id` MEDIUMINT,
  `shipment_id` VARCHAR(12),
  `seller_sku` VARCHAR(12),
  `cube` FLOAT,
  `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT TruckItems UNIQUE(`truck_id`, `shipment_id`, `seller_sku`, `cube`)
);