CREATE TABLE catalog_1564002580 (
`item-name` TEXT,
`item-description` TEXT,
`listing-id` TEXT,
`seller-sku` TEXT,
`price` DECIMAL(13,2),
`quantity` INT,
`open-date` TEXT,
`image-url` TEXT,
`item-is-marketplace` TEXT,
`product-id-type` INT,
`zshop-shipping-fee` TEXT,
`item-note` TEXT,
`item-condition` INT,
`zshop-category1` TEXT,
`zshop-browse-path` TEXT,
`zshop-storefront-feature` TEXT,
`asin1` TEXT NOT NULL,
`asin2` TEXT,
`asin3` TEXT,
`will-ship-internationally` TEXT,
`expedited-shipping` TEXT,
`zshop-boldface` TEXT,
`product-id` TEXT,
`bid-for-featured-placement` TEXT,
`add-delete` TEXT,
`pending-quantity` TEXT,
`fulfillment-channel` TEXT,
`merchant-shipping-group` TEXT,
`status` TEXT
);

CREATE USER 'asin-in-catalog' IDENTIFIED BY 'hidethispasswordplease';
GRANT alter,create,delete,index,insert,select,update,create ON catalog.* TO 'asin-in-catalog';
