# Setup from cmd line

Neither Dockerfile nor image, just commands:
```
docker pull mysql
docker run --name=catalog_db -e MYSQL_ROOT_PASSWORD=your-password-here -d mysql
docker inspect catalog_db
docker exec -it catalog_db bash
mysql -h localhost -u root -p < init.sql
```

# Todo
* [hide creds](https://docs.docker.com/samples/library/mysql/)

# References
[Setup MySQL](https://medium.com/coderscorner/connecting-to-mysql-through-docker-997aa2c090cc)
