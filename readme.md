# How to run the Shipment Tool
1. Install [docker](https://hub.docker.com/?overlay=onboarding)
2. Get the code from the Git repository
```
git clone git@bitbucket.org:jayfranco/asin-in-catalog.git
```
3. Run Docker
If Windows, use [PowerShell](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-windows-powershell?view=powershell-6) (already install in Win7+) or [Git Bash](https://gitforwindows.org) (which in turn uses [MingW](https://sourceforge.net/projects/mingw-w64/))
```
docker-compose up
docker exec -it `docker ps -q -n1` bash
python ONT8.py
```

# Notes
* Script only reads (and writes to) the sheet "Send These Items" sheet from ONT8.xlsx.
* Pre-requisite is open `//jfnt01/dash/dash03.xls`, save as `dash.xlsx`